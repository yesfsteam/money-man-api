﻿﻿using MoneyMan.Api.Models.Enums;

 namespace MoneyMan.Api.Extensions
{
    public static class IntExtensions
    {
        public static MoneyManStaff? ToMoneyManEmployerStaff(this int? employerStaff)
        {
            if (!employerStaff.HasValue)
                return null;
            var staff = employerStaff.Value;
            if (staff <= 10)
                return MoneyManStaff.Ten;
            if (staff > 10 && staff <= 50)
                return MoneyManStaff.Fifty;
            if (staff > 50 && staff <= 100)
                return MoneyManStaff.Hundred;
            if (staff > 100 && staff <= 1000)
                return MoneyManStaff.Thousand;
            return MoneyManStaff.MoreThanThousand;
        }
    }
}