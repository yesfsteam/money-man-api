﻿using System;
using MoneyMan.Api.Models.Enums;

namespace MoneyMan.Api.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToMoneyManDateFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
        
        public static string ToMoneyManDateFormat(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("yyyy-MM-dd") : string.Empty;
        }
        
        public static MoneyManExperience? ToMoneyManExperience(this DateTime? employeeStartDate)
        {
            if (!employeeStartDate.HasValue)
                return null;
            var days = (DateTime.Now - employeeStartDate.Value).TotalDays;
            if (days < 182)
                return MoneyManExperience.SixMonth;
            if (days >= 182 && days < 366)
                return MoneyManExperience.OneYear;
            if (days >= 366 && days <= 1826)
                return MoneyManExperience.FiveYears;
            return MoneyManExperience.MoreThanFiveYears;
        }
    }
}