﻿using System;
using MoneyMan.Api.Models.Enums;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;

namespace MoneyMan.Api.Extensions
{
    public static class EnumExtensions
    {
        public static MoneyManSex? ToMoneyManSex(this Gender? gender)
        {
            if (!gender.HasValue)
                return null;
            switch (gender.Value)
            {
                case Gender.Male:
                    return MoneyManSex.Male;
                case Gender.Female:
                    return MoneyManSex.Female;
                default:
                    throw new ArgumentException($"Invalid {nameof(gender)} value", nameof(gender));
            }
        }
        
        public static MoneyManMaritalStatus? ToMoneyManMaritalStatus(this MaritalStatus? maritalStatus)
        {
            if (!maritalStatus.HasValue)
                return null;
            switch (maritalStatus.Value)
            {
                case MaritalStatus.Single:
                    return MoneyManMaritalStatus.Single;
                case MaritalStatus.Married:
                    return MoneyManMaritalStatus.Married;
                case MaritalStatus.Widowed:
                    return MoneyManMaritalStatus.Widowed;
                case MaritalStatus.Divorced:
                    return MoneyManMaritalStatus.Divorced;
                case MaritalStatus.CivilMarriage:
                    return MoneyManMaritalStatus.CivilMarriage;
                default:
                    throw new ArgumentException($"Invalid {nameof(maritalStatus)} value", nameof(maritalStatus));
            }
        }
        
        public static MoneyManEmployerIndustry? ToMoneyManEmployerIndustry(this EmployerIndustry? employerIndustry)
        {
            if (!employerIndustry.HasValue)
                return null;
            switch (employerIndustry.Value)
            {
                case EmployerIndustry.Banks:
                    return MoneyManEmployerIndustry.Banks;
                case EmployerIndustry.Military:
                    return MoneyManEmployerIndustry.Military;
                case EmployerIndustry.CivilService:
                    return MoneyManEmployerIndustry.CivilService;
                case EmployerIndustry.Mining:
                    return MoneyManEmployerIndustry.Mining;
                case EmployerIndustry.Healthcare:
                    return MoneyManEmployerIndustry.Healthcare;
                case EmployerIndustry.InformationTechnology:
                    return MoneyManEmployerIndustry.InformationTechnology;
                case EmployerIndustry.ScienceEducation:
                    return MoneyManEmployerIndustry.ScienceEducation;
                case EmployerIndustry.Manufacturing:
                    return MoneyManEmployerIndustry.Manufacturing;
                case EmployerIndustry.Development:
                    return MoneyManEmployerIndustry.Development;
                case EmployerIndustry.Trading:
                    return MoneyManEmployerIndustry.Trading;
                case EmployerIndustry.Transport:
                    return MoneyManEmployerIndustry.Transport;
                case EmployerIndustry.CharityAndReligiousOrganizations:
                case EmployerIndustry.Gambling:
                case EmployerIndustry.HumanResources:
                case EmployerIndustry.Marketing:
                case EmployerIndustry.Restaurants:
                case EmployerIndustry.Agriculture:
                case EmployerIndustry.Insurance:
                case EmployerIndustry.Tourism:
                case EmployerIndustry.LegalServices:
                case EmployerIndustry.Other:
                    return MoneyManEmployerIndustry.Other;
                default:
                    throw new ArgumentException($"Invalid {nameof(employerIndustry)} value", nameof(employerIndustry));
            }
        }
        
        public static MoneyManEducation? ToMoneyManEducation(this Education? education)
        {
            if (!education.HasValue)
                return null;
            switch (education.Value)
            {
                case Education.PrimarySchool:
                    return MoneyManEducation.None;
                case Education.HighSchool:
                    return MoneyManEducation.HighSchool;
                case Education.SpecializedHighSchool:
                    return MoneyManEducation.SpecializedHighSchool;
                case Education.IncompleteSecondaryEducation:
                    return MoneyManEducation.IncompleteHighSchool;
                case Education.IncompleteHigherEducation:
                    return MoneyManEducation.IncompleteHigherEducation;
                case Education.HigherEducation:
                    return MoneyManEducation.HigherEducation;
                case Education.TwoOrMoreHigherEducations:
                case Education.AcademicDegree:
                    return MoneyManEducation.Other;
                default:
                    throw new ArgumentException($"Invalid {nameof(education)} value", nameof(education));
            }
        }
    }
}