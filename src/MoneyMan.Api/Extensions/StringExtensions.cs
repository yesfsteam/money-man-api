﻿﻿namespace MoneyMan.Api.Extensions
{
    public static class StringExtensions
    {
        public static string ToMoneyManPhoneFormat(this string phoneNumber)
        {
            return string.IsNullOrWhiteSpace(phoneNumber) ? null : $"+{phoneNumber}";
        }
    }
}