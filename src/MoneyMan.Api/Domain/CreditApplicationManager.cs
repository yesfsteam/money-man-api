﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MoneyMan.Api.Data;
using MoneyMan.Api.Extensions;
using MoneyMan.Api.Models;
using MoneyMan.Api.Models.Configuration;
using MoneyMan.Api.Models.Enums;
using SD.Cqrs;
using SD.Messaging.Models;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;
using Yes.Sms.Api.Contracts.Commands;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace MoneyMan.Api.Domain
{
    public interface ICreditApplicationManager
    {
        Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event);
       // Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model);
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly ICommandSender commandSender;
        private readonly IMoneyManClient client;
        private readonly ApplicationConfiguration configuration;
        private readonly MessagingConfiguration messagingConfiguration;
        private const string SERVER_UNEXPECTED_ERROR_DETAILS = "Детали ошибки в логах приложения";

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            ICommandSender commandSender, IMoneyManClient client, ApplicationConfiguration configuration,
            MessagingConfiguration messagingConfiguration)
        {
            this.logger = logger;
            this.repository = repository;
            this.commandSender = commandSender;
            this.client = client;
            this.configuration = configuration;
            this.messagingConfiguration = messagingConfiguration;
        }

        public async Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (@event.CreditAmount > configuration.MaxCreditAmount)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, $"Максимальная сумма кредита: {configuration.MaxCreditAmount}");
                    logger.LogInformation($"CheckCreditApplication. Skip. Credit amount too large. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                if (@event.ProfileType == ProfileType.Short)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, "Заявка не отправляется для короткой анкеты");
                    logger.LogInformation($"CheckCreditApplication. Skip. Short profile. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                
                var request = createRequest(@event);
                var response = await client.ClientPrescoring(request);
                
                await repository.SavePrescoringRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = @event.CreditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    MoneyManRequestId = response.Content?.RequestId,
                    MoneyManStatusCode = response.Content?.StatusCode,
                    MoneyManStatusText = response.Content?.StatusText,
                    MoneyManAutoLoginLink = response.Content?.AutoLoginLink
                });
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content.StatusCode == MoneyManFastCheckStatusCode.Ok)
                    {
                        var smsMessage = sendSms(request.Phone, response.Content.AutoLoginLink);
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Confirmed, @event.CreditApplicationId, response.Content.AutoLoginLink);
                        logger.LogInformation($"CheckCreditApplication. Check request approved. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}, Sms: {smsMessage}");
                        return ProcessingResult.Ok();
                    }
                    if (response.Content.StatusCode == MoneyManFastCheckStatusCode.Cancelled)
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Rejected, @event.CreditApplicationId);
                        logger.LogInformation($"CheckCreditApplication. Check request declined. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                        return ProcessingResult.Ok();
                    }
                    else
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, @event.CreditApplicationId, response.Content.ToJsonString());
                        logger.LogInformation($"CheckCreditApplication. Check request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                        return ProcessingResult.Ok();    
                    }
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogInformation($"CheckCreditApplication. Check request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogError($"CheckCreditApplication. Check request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                        $"Количество отправленных запросов: {messagingConfiguration.MaxRetryCount}. Последняя ошибка: {response.ErrorMessage}");
                    logger.LogWarning($"CheckCreditApplication. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                // Переповтор
                logger.LogWarning($"CheckCreditApplication. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}), try again later. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                        $"Количество попыток обработки: {messagingConfiguration.MaxRetryCount}. {SERVER_UNEXPECTED_ERROR_DETAILS}");
                    logger.LogError(e, $"CheckCreditApplication. Error while processing check request. Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                logger.LogError(e, $"CheckCreditApplication. Error while processing check request, try again later. Duration: {beginTime.GetDuration()} Event: {@event}");
                return ProcessingResult.Fail();
            }
        }
/*
        public async Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (model.CreditAmount > configuration.MaxCreditAmount)
                {
                    logger.LogInformation($"ResendCreditApplicationRequest. Skip. Credit amount too large. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, $"Максимальная сумма кредита: {configuration.MaxCreditAmount}");
                }
                var request = createRequest(model);
                var response = await client.ClientFastCheck(request);

                await repository.SaveFastCheckRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = creditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    MoneyManRequestId = response.Content?.RequestId,
                    MoneyManStatusCode = response.Content?.StatusCode,
                    MoneyManStatusText = response.Content?.StatusText
                });
                
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content.StatusCode == MoneyManFastCheckStatusCode.Ok)
                    {
                        logger.LogInformation($"ResendCreditApplicationRequest. Check request approved. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, Response: {response.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Approved);
                    }
                    if (response.Content.StatusCode == MoneyManFastCheckStatusCode.Cancelled)
                    {
                        logger.LogInformation($"ResendCreditApplicationRequest. Check request declined. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, Response: {response.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Declined);
                    }
                    
                    logger.LogInformation($"ResendCreditApplicationRequest. Check request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, Response: {response.Content}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, response.Content.ToJsonString());
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.LogInformation($"ResendCreditApplicationRequest. Check request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, response.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    logger.LogInformation($"ResendCreditApplicationRequest. Check request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorCheck, response.ErrorMessage);
                }

                logger.LogWarning($"ResendCreditApplicationRequest. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ResendCreditApplicationRequest. Error while processing check request. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }
*/
        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {/*
                var checkRequest = await repository.GetLastCheckRequest(creditApplicationId);
                if (checkRequest == null)
                {
                    logger.LogWarning($"ConfirmCreditApplication. Check request not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, "Запрос на проверку данных клиента не найден");
                }

                if (checkRequest.MoneyManStatusCode != MoneyManFastCheckStatusCode.Ok)
                {
                    logger.LogWarning($"ConfirmCreditApplication. Check request not successfull. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}, CheckRequest: {checkRequest}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, "Запрос на проверку данных клиента - неуспешный");
                }
                */
                if (model.ProfileType == ProfileType.Short)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Skip. Short profile. Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, "Заявка не отправляется для короткой анкеты");
                }

                var request = createRequest(model);
                var response = await client.ClientPrescoring(request);
                
                await repository.SavePrescoringRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = creditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    MoneyManRequestId = response.Content?.RequestId,
                    MoneyManStatusCode = response.Content?.StatusCode,
                    MoneyManStatusText = response.Content?.StatusText,
                    MoneyManAutoLoginLink = response.Content?.AutoLoginLink
                });
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content.StatusCode == MoneyManFastCheckStatusCode.Ok)
                    {
                        var smsMessage = sendSms(request.Phone, response.Content.AutoLoginLink);
                        logger.LogInformation($"ConfirmCreditApplication. Confirmation request confirmed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, Response: {response.Content}, Sms: {smsMessage}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Confirmed, $"Клиенту отправлена SMS: {smsMessage}");
                    }
                    if (response.Content.StatusCode == MoneyManFastCheckStatusCode.Cancelled)
                    {
                        logger.LogInformation($"ConfirmCreditApplication. Confirmation request rejected. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, Response: {response.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Rejected);
                    }

                    logger.LogInformation($"ConfirmCreditApplication. Confirmation request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, Response: {response.Content}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, response.Content.ToJsonString());
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Confirmation request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, response.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    logger.LogError($"ConfirmCreditApplication. Confirmation request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, response.ErrorMessage);
                }

                logger.LogWarning($"ConfirmCreditApplication. Confirmation request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ConfirmCreditApplication. Error while processing confirmation request. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }

        private string sendSms(string phoneNumber, string url)
        {
            var command = new SendSmsCommand
            {
                PnoneNumber = phoneNumber,
                Message = configuration.ConfirmationSmsTemplate.Replace("{URL}", url)
            };
            commandSender.SendCommand(command, BoundedContexts.SMS_API);
            return command.Message;
        }

        private ClientPrescoringRequest createRequest(CreditApplicationRequest request)
        {
            var timestamp = DateTimeOffset.Now.ToUnixTimeSeconds();
            var isResidenceAddressEmpty = request.IsResidenceAddressEmpty();
            var registrationAddress = new Address
            {
                RegionCode = getRegionCode(request.RegistrationAddressRegionKladrCode),
                RegionName = request.RegistrationAddressRegion,
                CityName = request.RegistrationAddressCity,
                StreetName = request.RegistrationAddressStreet,
                House = request.RegistrationAddressHouse,
                Building = request.RegistrationAddressBuilding,
                Block = request.RegistrationAddressBlock,
                Flat = request.RegistrationAddressApartment,
                AddressFullText = getRegistrationAddress(request)
            };
            return new ClientPrescoringRequest
            {
                Timestamp = timestamp,
                PartnerId = configuration.MoneyManPartnerId,
                Password = getPasswordHash(timestamp),
                //  RequestId = checkRequest.MoneyManRequestId,
                Phone = request.PhoneNumber.ToMoneyManPhoneFormat(),
                FirstName = request.FirstName,
                SecondName = request.MiddleName,
                LastName = request.LastName,
                Sex = request.Gender.ToMoneyManSex(),
                Email = request.Email,
                PassportSeries = request.PassportSeries,
                PassportNumber = request.PassportNumber,
                PassportIssueDate = request.PassportIssueDate.ToMoneyManDateFormat(),
                PassportAuthority = request.PassportIssuer,
                PassportCodeDevision = getPassportDepartmentCode(request.PassportDepartmentCode),
                MaritalStatus = request.MaritalStatus.ToMoneyManMaritalStatus(),
                Birthday = request.DateOfBirth.ToMoneyManDateFormat(),
                Birthplace = request.PlaceOfBirth,
                UserPdConsentDate = request.PersonalDataProcessApproveDate.ToMoneyManDateFormat(),
                UserBchConsentDate = request.CreditBureauProcessApproveDate.ToMoneyManDateFormat(),
                Salary = request.MonthlyIncome?.ToString(CultureInfo.InvariantCulture),
                CreditAmount = request.CreditAmount ?? 0,
                CreditTerm = request.CreditPeriod / 30 ?? 0,
                RegAddress = registrationAddress,
                FactAddress = isResidenceAddressEmpty
                    ? registrationAddress
                    : new Address
                    {
                        RegionCode = getRegionCode(request.ResidenceAddressRegionKladrCode),
                        RegionName = request.ResidenceAddressRegion,
                        CityName = request.ResidenceAddressCity,
                        StreetName = request.ResidenceAddressStreet,
                        House = request.ResidenceAddressHouse,
                        Building = request.ResidenceAddressBuilding,
                        Block = request.ResidenceAddressBlock,
                        Flat = request.ResidenceAddressApartment,
                        AddressFullText = getResidenceAddress(request)
                    },
                JobType = request.EmployerIndustry.ToMoneyManEmployerIndustry(),
                JobPosition = request.EmployeePosition?.GetDescription(),
                JobAddress = request.Activity == Activity.Employee
                    ? new ShortAddress
                    {
                        RegionCode = getRegionCode(request.EmployerAddressKladrCode),
                        AddressFullText = getEmployerAddress(request)
                    }
                    : null,
                JobOrgName = request.EmployerName,
                JobPhone = request.EmployerPhoneNumber.ToMoneyManPhoneFormat(),
                JobExperience = request.EmployeeStartDate.ToMoneyManExperience(),
                JobEmployees = request.EmployerStaff.ToMoneyManEmployerStaff(),
                Education = request.Education.ToMoneyManEducation()
            };
        }
        
        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, CreditOrganizationRequestStatus status, Guid creditApplicationId, string details = null)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private Response<CreditApplicationResponse> createResponse(DateTime date, CreditOrganizationRequestStatus status, string details = null)
        {
            return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
            {
                Date = date,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            });
        }

        private string getPasswordHash(long timestamp)
        {
            using var sha1 = new SHA1Managed();
            var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes($"{configuration.MoneyManPassword}{timestamp}"));
            var sb = new StringBuilder(hash.Length * 2);
            foreach (var b in hash)
                sb.Append(b.ToString("X2"));
            return sb.ToString();
        }
        
        private int getRegionCode(string regionCodeKladr)
        {
            if (string.IsNullOrWhiteSpace(regionCodeKladr) || regionCodeKladr.Length < 2)
                return 0;
            var regionCodeString = regionCodeKladr.Substring(0, 2);
            return !int.TryParse(regionCodeString, out var regionCode) ? 0 : regionCode;
        }
        
        private string getRegistrationAddress(CreditApplicationRequest model)
        {
            return $"{model.RegistrationAddressRegion} {model.RegistrationAddressCity} {model.RegistrationAddressStreet} {model.RegistrationAddressHouse} {model.RegistrationAddressBlock} {model.RegistrationAddressBuilding} {model.RegistrationAddressApartment}";
        }
        
        private string getPassportDepartmentCode(string passportDepartmentCode)
        {
            return string.IsNullOrWhiteSpace(passportDepartmentCode) ? null : passportDepartmentCode.Replace("-", string.Empty);
        }
        
        private string getResidenceAddress(CreditApplicationRequest model)
        {
            return $"{model.ResidenceAddressRegion} {model.ResidenceAddressCity} {model.ResidenceAddressStreet} {model.ResidenceAddressHouse} {model.ResidenceAddressBlock} {model.ResidenceAddressBuilding} {model.ResidenceAddressApartment}";
        }
        
        private string getEmployerAddress(CreditApplicationRequest model)
        {
            return $"{model.EmployerAddressRegion} {model.EmployerAddressCity} {model.EmployerAddressStreet} {model.EmployerAddressHouse} {model.EmployerAddressBlock} {model.EmployerAddressBuilding} {model.EmployerAddressApartment}";
        }
    }
}