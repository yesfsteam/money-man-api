﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using MoneyMan.Api.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Yes.Infrastructure.Http;

namespace MoneyMan.Api.Domain
{
    public interface IMoneyManClient
    {
        Task<Response<ClientFastCheckResponse>> ClientFastCheck(ClientFastCheckRequest request);
        Task<Response<ClientPrescoringResponse>> ClientPrescoring(ClientPrescoringRequest request);
        Task<Response<CheckApplicationStatusResponse>> CheckApplicationStatus(CheckApplicationStatusRequest request);
    }

    public class MoneyManClient : IMoneyManClient
    {
        private readonly HttpClient httpClient;
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public MoneyManClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
            jsonSerializerSettings = new JsonSerializerSettings{ContractResolver = new CamelCasePropertyNamesContractResolver()};
        }

        public async Task<Response<ClientFastCheckResponse>> ClientFastCheck(ClientFastCheckRequest request)
        {
            return await post<ClientFastCheckResponse>("client-fast-check", request);
        } 
        
        public async Task<Response<ClientPrescoringResponse>> ClientPrescoring(ClientPrescoringRequest request)
        {
            return await post<ClientPrescoringResponse>("client-prescoring", request);
        }

        public async Task<Response<CheckApplicationStatusResponse>> CheckApplicationStatus(CheckApplicationStatusRequest request)
        {
            return await post<CheckApplicationStatusResponse>("check-application-status", request);
        }

        private async Task<Response<T>> post<T>(string uri, object obj)
        {
            var requestUri = new Uri(httpClient.BaseAddress, uri);
            var content = new JsonContent(obj, jsonSerializerSettings);
            var response = await httpClient.PostAsync(requestUri, content);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> createResponse<T>(HttpResponseMessage response)
        {
            var result = new Response<T>
            {
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                StatusCode = response.StatusCode
            };
            
            var content = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
                result.Content = JsonConvert.DeserializeObject<T>(content);
            else
                result.ErrorMessage = content;
            
            return result;
        }
    }
}