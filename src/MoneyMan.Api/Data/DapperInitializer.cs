﻿using Dapper;

namespace MoneyMan.Api.Data
{
    public static class DapperInitializer
    {
        public static void ConfigureDapper()
        {
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }
    }
}