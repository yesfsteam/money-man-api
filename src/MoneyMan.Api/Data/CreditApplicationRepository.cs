﻿using System;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using MoneyMan.Api.Models;
using Npgsql;

namespace MoneyMan.Api.Data
{
    public interface ICreditApplicationRepository
    {
        Task SaveFastCheckRequest(CreditApplicationRequestModel model);
        Task<CreditApplicationRequestModel> GetLastCheckRequest(Guid creditApplicationId);
        Task SavePrescoringRequest(CreditApplicationRequestModel model);
    }

    public class CreditApplicationRepository : ICreditApplicationRepository
    {
        private readonly IConfiguration configuration;

        public CreditApplicationRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task SaveFastCheckRequest(CreditApplicationRequestModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.MoneyManRequestId,
                model.MoneyManStatusCode,
                model.MoneyManStatusText
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_check_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    money_man_request_id,
                    money_man_status_code,
                    money_man_status_text
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :moneyManRequestId,
                    :moneyManStatusCode,
                    :moneyManStatusText
                );", queryParams);
        }

        public async Task<CreditApplicationRequestModel> GetLastCheckRequest(Guid creditApplicationId)
        {
            await using var connection = createConnection();
            return await connection.QueryFirstOrDefaultAsync<CreditApplicationRequestModel>($@"
                SELECT
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    money_man_request_id,
                    money_man_status_code,
                    money_man_status_text
                FROM credit_application_check_request
                WHERE
                    credit_application_id = :creditApplicationId::uuid
                ORDER BY
                    created_date DESC;", new {creditApplicationId});
        }

        public async Task SavePrescoringRequest(CreditApplicationRequestModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.MoneyManRequestId,
                model.MoneyManStatusCode,
                model.MoneyManStatusText,
                model.MoneyManAutoLoginLink
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                 INSERT INTO credit_application_confirm_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    money_man_request_id,
                    money_man_status_code,
                    money_man_status_text,
                    money_man_auto_login_link
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :moneyManRequestId,
                    :moneyManStatusCode,
                    :moneyManStatusText,
                    :moneyManAutoLoginLink
                );", queryParams);
        }

        private NpgsqlConnection createConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}