﻿namespace MoneyMan.Api.Models
{
    public class ShortAddress
    {
        public int RegionCode { get; set; }
        
        public string AddressFullText { get; set; }
    }
}