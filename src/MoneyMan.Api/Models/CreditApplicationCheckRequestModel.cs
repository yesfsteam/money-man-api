﻿using System;
using System.Net;
using MoneyMan.Api.Models.Enums;
using Yes.Infrastructure.Common.Models;

namespace MoneyMan.Api.Models
{
    public class CreditApplicationCheckRequestModel : JsonModel
    {
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Http статус запроса из MoneyMan
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Идентификатор запроса в MoneyMan
        /// </summary>
        public string RequestId { get; set; }
        
        /// <summary>
        /// Статус запроса из MoneyMan
        /// </summary>
        public MoneyManCheckCreditApplicationStatusCode? StatusCode { get; set; }
        
        /// <summary>
        /// Описание ошибки из MoneyMan
        /// </summary>
        public string StatusText { get; set; }
        
        /// <summary>
        /// Дата и время перехода заявки в текущий статус
        /// </summary>
        public DateTime? StatusTime { get; set; }
    }
}