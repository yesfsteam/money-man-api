﻿﻿﻿using System.ComponentModel;

  namespace MoneyMan.Api.Models.Enums
{
    public enum MoneyManExperience
    {
        [Description("До 6 месяцев")]
        SixMonth = 0,
        [Description("От 6 месяцев до года")]
        OneYear = 1,
        [Description("От 1 года до 5 лет")]
        FiveYears = 2,
        [Description("Более 5 лет")]
        MoreThanFiveYears = 3
    }
}