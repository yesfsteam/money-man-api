﻿﻿﻿using System.ComponentModel;

  namespace MoneyMan.Api.Models.Enums
{
    public enum MoneyManStaff
    {
        [Description("1-10")]
        Ten = 0,
        [Description("11-50")]
        Fifty = 1,
        [Description("51-100")]
        Hundred = 2,
        [Description("101-1000")]
        Thousand = 3,
        [Description("Более 1000")]
        MoreThanThousand = 4
    }
}