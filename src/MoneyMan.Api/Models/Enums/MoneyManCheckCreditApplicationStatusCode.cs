﻿namespace MoneyMan.Api.Models.Enums
{
    public enum MoneyManCheckCreditApplicationStatusCode
    {
        /// <summary>
        /// Превышение допустимого количества запросов по одному request_id в день. Отправляется, если количество запросов по одному и тому же request_id за текущий день > 10
        /// </summary>
        TooManyRequests = -2,
        
        /// <summary>
        /// Произошла ошибка при обработке
        /// </summary>
        ProcessingError = -1,
        
        /// <summary>
        /// Отказано. Отправляется, если в выдаче займа было отказано.
        /// </summary>
        Cancelled = 0,
        
        /// <summary>
        /// Оформление в процессе. Данный статус отправляется с момента отправки автологинящей ссылки в методе clientPrescoring и до момента получения займа клиентом.
        /// </summary>
        InProgress = 1,
        
        /// <summary>
        /// Оформлено
        /// </summary>
        Issued = 2,
        
        /// <summary>
        /// На верификации
        /// </summary>
        LoanOnVerification = 3
    }
}