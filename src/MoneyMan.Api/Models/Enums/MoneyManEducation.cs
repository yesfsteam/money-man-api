﻿﻿using System.ComponentModel;

 namespace MoneyMan.Api.Models.Enums
{
    public enum MoneyManEducation
    {
        [Description("Отсутствует")]
        None = 0,
        [Description("Неполное среднее")]
        IncompleteHighSchool = 1,
        [Description("Среднее")]
        HighSchool = 2,
        [Description("Среднее (средне-специальное)")]
        SpecializedHighSchool = 3,
        [Description("Неполное высшее")]
        IncompleteHigherEducation = 4,
        [Description("Высшее")]
        HigherEducation = 5,
        [Description("Другое")]
        Other = 6
    }
}