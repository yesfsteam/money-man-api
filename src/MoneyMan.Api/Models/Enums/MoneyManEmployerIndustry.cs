﻿﻿﻿using System.ComponentModel;

  namespace MoneyMan.Api.Models.Enums
{
    public enum MoneyManEmployerIndustry
    {
        [Description("Армия")]
        Military = 0,
        [Description("Государственная служба")]
        CivilService = 1,
        [Description("Добывающий сектор")]
        Mining = 2,
        [Description("Информационные технологии/телекоммуникации")]
        InformationTechnology = 3,
        [Description("Культура и искусство")]
        CultureAndArt = 4,
        [Description("Медицина")]
        Healthcare = 5,
        [Description("Наука и образование")]
        ScienceEducation = 6,
        [Description("Охранная деятельность")]
        SecurityActivity = 7,
        [Description("Промышленность")]
        Manufacturing = 8,
        [Description("Сервис и услуги")]
        ServiceAndFacilities = 9,
        [Description("Строительство и недвижимость")]
        Development = 10,
        [Description("Торговля")]
        Trading = 11,
        [Description("Транспорт")]
        Transport = 12,
        [Description("ТЭК")]
        FuelAndEnergyComplex = 13,
        [Description("Финансы, банки, страхование, консалтинг")]
        Banks = 14,
        [Description("Другое")]
        Other = 15
    }
}