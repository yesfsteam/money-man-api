﻿using System.ComponentModel;

namespace MoneyMan.Api.Models.Enums
{
    public enum MoneyManMaritalStatus
    {
        [Description("Не женат/Не замужем")]
        Single = 0,
        [Description("Женат/Замужем")]
        Married = 1,
        [Description("Вдовец/Вдова")]
        Widowed = 2,
        [Description("Разведен/Разведена")]
        Divorced = 3,
        [Description("Гражданский брак")]
        CivilMarriage = 4
    }
}