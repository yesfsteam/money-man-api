﻿namespace MoneyMan.Api.Models.Enums
{
    public enum MoneyManFastCheckStatusCode
    {
        /// <summary>
        /// Оформление займа возможно
        /// </summary>
        Ok = 1,
        
        /// <summary>
        /// В возможности займа отказано, либо это уже 3+ запрос с одинаковыми параметрами клиента
        /// </summary>
        Cancelled = 0,
        
        /// <summary>
        /// Произошла ошибка при обработке
        /// </summary>
        ProcessingError = -1
    }
}