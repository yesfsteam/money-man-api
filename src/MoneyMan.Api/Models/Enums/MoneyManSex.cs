﻿namespace MoneyMan.Api.Models.Enums
{
    public enum MoneyManSex
    {
        Male = 0,
        Female = 1
    }
}