﻿using System;

namespace MoneyMan.Api.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public long MoneyManPartnerId { get; set; }
        public string MoneyManPassword { get; set; }
        public Guid CreditOrganizationId { get; set; }
        public int MaxCreditAmount { get; set; }
        public string ConfirmationSmsTemplate { get; set; }
    }
}