﻿using System;
using System.Net;
using MoneyMan.Api.Models.Enums;
using Yes.Infrastructure.Common.Models;

namespace MoneyMan.Api.Models
{
    public class CreditApplicationRequestModel : JsonModel
    {
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Http статус запроса
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// Идентификатор запроса в MoneyMan
        /// </summary>
        public string MoneyManRequestId { get; set; }
        
        /// <summary>
        /// Статус запроса из MoneyMan
        /// </summary>
        public MoneyManFastCheckStatusCode? MoneyManStatusCode { get; set; }
        
        /// <summary>
        /// Описание ошибки из MoneyMan
        /// </summary>
        public string MoneyManStatusText { get; set; }
        
        /// <summary>
        /// Ссылка для пользователя
        /// </summary>
        public string MoneyManAutoLoginLink { get; set; }
    }
}