﻿using Yes.Infrastructure.Common.Models;

namespace MoneyMan.Api.Models
{
    public class ClientFastCheckRequest : JsonModel
    {
        public long Timestamp { get; set; }
        
        public long PartnerId { get; set; }
        
        //public string Wmid { get; set; }
        
        public string Password { get; set; }
        
        public string Phone { get; set; }
        
        public string FirstName { get; set; }
        
        public string SecondName { get; set; }
        
        public string LastName { get; set; }
        
        public string PassportSeries { get; set; }
        
        public string PassportNumber { get; set; }
        
        public string Birthday { get; set; }
        
        public string UserPdConsentDate { get; set; }
    }
}