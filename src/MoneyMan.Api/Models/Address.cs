﻿namespace MoneyMan.Api.Models
{
    public class Address : ShortAddress
    {
        public string RegionType { get; set; }
        
        public string RegionName { get; set; }
        
        public string DistrictType { get; set; }
        
        public string DistrictName { get; set; }
        
        public string CityType { get; set; }
        
        public string CityName { get; set; }
        
        public string StreetType { get; set; }
        
        public string StreetName { get; set; }
        
        public string House { get; set; }
        
        public string Building { get; set; }
        
        public string Block { get; set; }
        
        public string Flat { get; set; }
    }
}