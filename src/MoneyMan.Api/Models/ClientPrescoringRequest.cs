﻿using MoneyMan.Api.Models.Enums;
using Yes.Infrastructure.Common.Models;

namespace MoneyMan.Api.Models
{
    public class ClientPrescoringRequest : JsonModel
    {
        public long Timestamp { get; set; }
        
        public long PartnerId { get; set; }
        
        //public string Wmid { get; set; }
        
        public string Password { get; set; }
        
        //public string RequestId { get; set; }
        
        public string Phone { get; set; }
        
        public string FirstName { get; set; }
        
        public string SecondName { get; set; }
        
        public string LastName { get; set; }
        
        public MoneyManSex? Sex { get; set; }
        
        public string Email { get; set; }
        
        public string PassportSeries { get; set; }
        
        public string PassportNumber { get; set; }
        
        public string PassportIssueDate { get; set; }
        
        public string PassportAuthority { get; set; }
        
        public string PassportCodeDevision { get; set; }
        
        public MoneyManMaritalStatus? MaritalStatus { get; set; }
        
        public string Birthday { get; set; }
        
        public string Birthplace { get; set; }
        
        public string UserPdConsentDate { get; set; }
        
        public string UserBchConsentDate { get; set; }
        
        public string Salary { get; set; }
        
        public decimal CreditAmount { get; set; }
        
        public int CreditTerm { get; set; }
        
        public Address RegAddress { get; set; }
        
        public Address FactAddress { get; set; }
        
        public MoneyManEmployerIndustry? JobType { get; set; }
        
        public string JobPosition { get; set; }
        
        public ShortAddress JobAddress { get; set; }
        
        public string JobOrgName { get; set; }
        
        public string JobPhone { get; set; }
        
        public MoneyManExperience? JobExperience { get; set; }
        
        public MoneyManStaff? JobEmployees { get; set; }
        
        public MoneyManEducation? Education { get; set; }
    }
}