﻿using Yes.Infrastructure.Common.Models;

namespace MoneyMan.Api.Models
{
    public class CheckApplicationStatusRequest : JsonModel
    {
        public long Timestamp { get; set; }
        
        public long PartnerId { get; set; }
        
        //public string Wmid { get; set; }
        
        public string Password { get; set; }
        
        public string RequestId { get; set; }
    }
}