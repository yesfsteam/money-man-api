﻿using MoneyMan.Api.Models.Enums;
using Yes.Infrastructure.Common.Models;

namespace MoneyMan.Api.Models
{
    public class ClientFastCheckResponse : JsonModel
    {
        public string RequestId { get; set; }
        
        public MoneyManFastCheckStatusCode StatusCode { get; set; }
        
        public string StatusText { get; set; }
    }
}