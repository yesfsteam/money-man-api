﻿namespace MoneyMan.Api.Models
{
    public class ClientPrescoringResponse : ClientFastCheckResponse
    {
        public string AutoLoginLink { get; set; }
    }
}