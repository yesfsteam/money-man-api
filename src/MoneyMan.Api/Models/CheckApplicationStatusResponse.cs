﻿using System;
using MoneyMan.Api.Models.Enums;
using Yes.Infrastructure.Common.Models;

namespace MoneyMan.Api.Models
{
    public class CheckApplicationStatusResponse : JsonModel
    {
        public string RequestId { get; set; }
        
        public MoneyManCheckCreditApplicationStatusCode StatusCode { get; set; }
        
        public string StatusText { get; set; }
        
        public DateTime StatusTime { get; set; }
    }
}