﻿using System.Threading.Tasks;
using MoneyMan.Api.Domain;
using SD.Cqrs;
using SD.Messaging.Models;
using Yes.CreditApplication.Api.Contracts.Events;

namespace MoneyMan.Api.Messaging
{
    public class CreditApplicationProjection : Projection
    {
        private readonly ICreditApplicationManager manager;

        public CreditApplicationProjection(ICreditApplicationManager manager) 
        {
            this.manager = manager; 
        }

        public async Task<ProcessingResult> Handle(CreditApplicationCreatedEvent @event)
            => await manager.CheckCreditApplication(@event);
    }
}